import { NgModule, Provider } from "@angular/core";
import {BrowserModule} from '@angular/platform-browser';
import {RouterOutlet} from "@angular/router";
import { registerLocaleData } from "@angular/common";
import ruLocale from '@angular/common/locales/ru';

import {
    HTTP_INTERCEPTORS,
    HttpClientModule
} from "@angular/common/http";
import {AppComponent} from './app.component';
import {MainLayoutComponent} from './shared/components/main-layout/main-layout.component';
import {HomePageComponent} from './home-page/home-page.component';
import {PostPageComponent} from './post-page/post-page.component';
import {AppRoutingModule} from "./app-routing.module";
import { PostComponent } from './shared/components/post/post.component';
import { AuthInterceptor } from "./shared/auth.interceptor";
import { QuillViewHTMLComponent } from "ngx-quill";
import { RxJsHomeComponent } from './rxJs/rx-js-home/rx-js-home.component';

registerLocaleData(ruLocale, 'ru');

const INTERCEPTOR_PROVIDER: Provider = {
    provide: HTTP_INTERCEPTORS,
    multi: true,
    useClass: AuthInterceptor
}
@NgModule({
    declarations: [
        AppComponent,
        MainLayoutComponent,
        HomePageComponent,
        PostPageComponent,
        PostComponent,
        RxJsHomeComponent,
    ],
    imports: [
        BrowserModule,
        RouterOutlet,
        AppRoutingModule,
        HttpClientModule,
        QuillViewHTMLComponent
    ],
    providers: [
      INTERCEPTOR_PROVIDER
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
