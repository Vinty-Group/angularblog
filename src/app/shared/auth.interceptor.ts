import { inject, Injectable } from "@angular/core";
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { catchError, EMPTY, Observable, tap } from "rxjs";
import { AuthService } from "../admin/shared/services/auth.service";
import { Router } from "@angular/router";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  authService: AuthService = inject(AuthService);
  router: Router = inject(Router);

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (this.authService.isAuthenticated()) {
      request = request.clone({
        setParams: {
          auth: this.authService.token
        }
      });
    }
    return next.handle(request)
      .pipe(
        tap(() => {
        }),
        catchError((error: HttpErrorResponse) => {
          console.log("[Interceptor error]:", error);
          if (error.status === 401) {
            this.authService.logout();
            this.router.navigate(["/admin", "login"], {
              queryParams: {
                authFailed: true
              }
            });
          }
          return EMPTY;
        })
      );
  }
}
