import { Component, inject, OnInit } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { PostsService } from "../shared/services/posts.service";
import { Observable, switchMap } from "rxjs";
import { IPost } from "../admin/shared/interfaces";
import { Parser } from "@angular/compiler";

@Component({
  selector: "app-post-page",
  templateUrl: "./post-page.component.html",
  styleUrls: ["./post-page.component.css"]
})
export class PostPageComponent implements OnInit{
  post$: Observable<IPost>;

  route = inject(ActivatedRoute);
  postService = inject(PostsService);

  ngOnInit(): void {
    this.post$ = this.route.params
      .pipe(switchMap((params: Params) => {
        return this.postService.getById(params['id'])
      }))
  }
}
