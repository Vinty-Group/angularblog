import {NgModule} from '@angular/core';
import { RouterModule, Routes} from "@angular/router";
import {MainLayoutComponent} from "./shared/components/main-layout/main-layout.component";
import {HomePageComponent} from "./home-page/home-page.component";
import {PostPageComponent} from "./post-page/post-page.component";


const routes: Routes = [{
    path: '', component: MainLayoutComponent, children: [
        // при загрузке базовой страницы - редирект на HomePageComponent
        {path: '', pathMatch: 'full', component: HomePageComponent},
        {path: 'post/:id', component: PostPageComponent}
    ]
},
    // ленивая загрузка. Если в строке /admin -> Грузить молдуль AdminModule
    {path: 'admin', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule)},
]

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        // для роутинга на стороне Apache
        useHash: true
    })],
    exports: [RouterModule]
})
export class AppRoutingModule {

}
