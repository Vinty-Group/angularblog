import { Component, inject, OnInit } from "@angular/core";
import { PostsService } from "../shared/services/posts.service";
import { Observable } from "rxjs";
import { IPost } from "../admin/shared/interfaces";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit{

  posts$: Observable<IPost[]>;

  postService = inject(PostsService);
  ngOnInit(): void {
    this.posts$ = this.postService.getAll();
  }
}
