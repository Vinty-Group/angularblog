import { Component, inject } from "@angular/core";
import {Router} from "@angular/router";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.css']
})
export class AdminLayoutComponent {

  router: Router = inject(Router);
  authService: AuthService = inject(AuthService);

  logout(event: Event) {
    event.preventDefault();
    this.authService.logout();
  }
}
