import { Component, inject, OnDestroy, OnInit } from "@angular/core";
import { PostsService } from "../../shared/services/posts.service";
import { IPost } from "../shared/interfaces";
import { Subscription } from "rxjs";
import { AlertService } from "../shared/services/alert.service";

@Component({
  selector: "app-dashboard-page",
  templateUrl: "./dashboard-page.component.html",
  styleUrls: ["./dashboard-page.component.css"]
})
export class DashboardPageComponent implements OnInit, OnDestroy {

  posts: IPost[] =[];
  pSub: Subscription;
  dSub: Subscription;
  searchStr: string = '';

  postService = inject(PostsService);
  alertService = inject(AlertService);

  ngOnInit(): void {
    this.pSub = this.postService.getAll().subscribe(posts => {
      this.posts = posts;
    })
  }

  ngOnDestroy(): void {
    if (this.pSub){
      this.pSub.unsubscribe();
    }
    if (this.dSub){
      this.dSub.unsubscribe();
    }
  }

  remove(id: string) {
    this.dSub = this.postService.remove(id).subscribe(() => {
      this.posts = this.posts.filter(post => post.id !== id);
      this.alertService.warning('Пост был удален.')
    });
  }
}
