import { Component, inject, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { IUser } from "../shared/interfaces";
import { AuthService } from "../shared/services/auth.service";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { finalize } from "rxjs";

@Component({
  selector: "app-login-page",
  templateUrl: "./login-page.component.html",
  styleUrls: ["./login-page.component.css"]
})
export class LoginPageComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  message: string;

     router: Router = inject(Router);
     rout: ActivatedRoute = inject(ActivatedRoute);
     authService: AuthService = inject(AuthService);

  ngOnInit(): void {
    this.rout.queryParams.subscribe((params: Params) => {
      if (params["loginAgain"]) {
        this.message = "Пожалуйста введите данные.";
      } else if (params["authFailed"]) {
        this.message = "Сессия истекла. Введите данные заново.";
      }
    });
    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(6)
      ])
    });
  }

  submit() {
    if (this.form.invalid) {
      return;
    }
    this.submitted = true;
    const user: IUser = {
      email: this.form.value.email,
      password: this.form.value.password
    };

    this.authService.login(user)
      .pipe(finalize(() => this.submitted = false))
      .subscribe(() => {
        console.log(5555);
        this.router.navigate(["/admin", "dashboard"]);
        console.log(123123);
      });
  }
}
