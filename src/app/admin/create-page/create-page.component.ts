import { Component, inject, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { IPost } from "../shared/interfaces";
import { PostsService } from "../../shared/services/posts.service";
import { AlertService } from "../shared/services/alert.service";

@Component({
  selector: "app-create-page",
  templateUrl: "./create-page.component.html",
  styleUrls: ["./create-page.component.css"]
})
export class CreatePageComponent implements OnInit {
  form: FormGroup;
  fb = inject(FormBuilder);


  postService: PostsService = inject(PostsService);
  alertService: AlertService = inject(AlertService);

  ngOnInit(): void {
    this.form = this.fb.group({
      title: [null, Validators.required],
      text: [null, Validators.required],
      author: [null, Validators.required]
    });
  }

  submit() {
    if (this.form.invalid) {
      return;
    }
    const post: IPost = {
      title: this.form.value.title,
      author: this.form.value.author,
      text: this.form.value.text,
      date: new Date()
    };
    this.postService.create(post).subscribe(() => {
      this.form.reset();
      this.alertService.success("Пост был создан");
    });
  }
}
