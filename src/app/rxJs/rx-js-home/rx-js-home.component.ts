import { Component } from "@angular/core";
import { Subject, Subscription } from "rxjs";
import { logMessages } from "@angular-devkit/build-angular/src/builders/browser-esbuild/esbuild";

@Component({
  selector: "app-rx-js-home",
  templateUrl: "./rx-js-home.component.html",
  styleUrls: ["./rx-js-home.component.css"]
})

export class RxJsHomeComponent {

  iSub: Subscription;
  stream$: Subject<number> = new Subject<number>();
  counter = 0;
  constructor() {
  this.iSub = this.stream$.subscribe(value => {
    console.log('Subscribe', value);
  })

    // const intervalStream$ = interval(1000);
    //   this.iSub = intervalStream$
    //     .pipe(
    //       filter(value => value % 2 === 0),
    //       map((value) => `Mapped value ${value}`),
    //       // mergeMap
    //       // exhaustMap
    //       // concatMap
    //     )
    //     .subscribe((value) => {
    //       console.log(value);
    //     });
  }

  stopInterval() {
    this.iSub.unsubscribe();
  }

  next() {
    this.counter++;
    this.stream$.next(this.counter);
  }
}
